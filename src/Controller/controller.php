<?php

namespace Sda\ConstructionBidForm\Controller;

use Sda\ConstructionBidForm\Config\Routing;
use Sda\ConstructionBidForm\Request\Request;
use Sda\ConstructionBidForm\Response\Response;
use Sda\ConstructionBidForm\Session\Session;
use Sda\ConstructionBidForm\View\Template;
use Sda\ConstructionBidForm\WithRepositories\Groups\Group;
use Sda\ConstructionBidForm\WithRepositories\Groups\GroupRepository;
use Sda\ConstructionBidForm\WithRepositories\Offers\Offer;
use Sda\ConstructionBidForm\WithRepositories\Offers\OfferRepository;
use Sda\ConstructionBidForm\WithRepositories\Rows\Row;
use Sda\ConstructionBidForm\WithRepositories\Rows\RowRepository;
use Sda\ConstructionBidForm\WithRepositories\Users\Users;
use Sda\ConstructionBidForm\WithRepositories\Users\UsersRepository;
use Sda\ConstructionBidForm\WithRepositories\Users\RegisteredUsers;
use Sda\ConstructionBidForm\WithRepositories\Users\RegisteredUsersRepository;

class Controller
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Template
     */
    private $templateEngine;
    /**
     * @var UsersRepository
     */
    private $usersRepo;
    /**
     * @var RegisteredUsersRepository
     */
    private $registeredUsersRepo;
    /**
     * @var OfferRepository
     */
    private $offerRepository;
    /**
     * @var GroupRepository
     */
    private $groupRepository;
    /**
     * @var RowRepository
     */
    private $rowRepository;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var array
     */
    private $params =[];
    /**
     * @var string
     */
    private $templateName = '';


    /**
     * Controller constructor.
     * @param Request $request
     * @param Session $session
     * @param UsersRepository $usersRepo
     * @param RegisteredUsersRepository $registeredUsersRepo
     * @param OfferRepository $offerRepository
     * @param GroupRepository $groupRepository
     * @param RowRepository $rowRepository
     * @param Template $templateEngine
     */
    public function __construct
    (
        Request $request,
        Session $session,
        UsersRepository $usersRepo,
        RegisteredUsersRepository $registeredUsersRepo,
        OfferRepository $offerRepository,
        GroupRepository $groupRepository,
        RowRepository $rowRepository,
        Template $templateEngine
    )
    {
        $this->request = $request;
        $this->session = $session;
        $this->usersRepo = $usersRepo;
        $this->registeredUsersRepo = $registeredUsersRepo;
        $this->offerRepository = $offerRepository;
        $this->groupRepository = $groupRepository;
        $this->rowRepository = $rowRepository;
        $this->templateEngine = $templateEngine;
    }

    public function run()
    {
        $action = $this->request->getParamFromGet('action', Routing::INDEX);

        $username = $this->session->getFromSession('username');

        if ('' === $username)
        {
            $username = $this->request->getParamFromPost('username', '');
        }

        $this->params['username'] = $username;

        switch($action)
        {
            case Routing::INDEX :
                $this->templateName = 'index.tmpl.html';
                break;
            case Routing::INDEX_AJAX :
                $this->templateName = 'ajax_index.tmpl.html';
                break;
            case Routing::LOGIN :
                $this->templateName = 'login.tmpl.html';
                $this->login();
                break;
            case Routing::REGISTER :
                $this->templateName = 'register.tmpl.html';
                $this->register();
                break;
            case Routing::GUEST_PANEL :
                $userId = $this->registeredUsersRepo->getUserIdByName($username);
                $offers = $this->offerRepository->getOffersFromUser($userId);
                $this->params['offers'] = $offers;
                $this->templateName = 'offer_list.tmpl.html';
                break;
            case Routing::NEW_OFFER :
                $this->templateName = 'offer.tmpl.html';
                break;
            case Routing::SAVE_FORM :
                $userId = $this->registeredUsersRepo->getUserIdByName($username);
                $offer = new Offer(null, $userId, $_POST['offer_name']);
                $this->offerRepository->saveOffer($offer);
                foreach ($_POST['offerData'] as $groups)
                {
                    foreach ($groups as $key => $val)
                    {
                        if ('group_name' === $key)
                        {
                            $group = new Group(null, $offer->getId(), $val);
                            $this->groupRepository->saveGroup($group);
                            $neededGroupId = $group->getId();
                            $_SESSION['groupId'] = $neededGroupId;
                        } else
                        {
                            $name = '';
                            $quantity = 0;
                            $price = 0;
                            foreach ($val as $rowKey => $rowVal)
                            {
                                if ('name' === $rowKey)
                                {
                                    if($rowVal === '')
                                    {
                                        $message = "forgot to name on of rows";
                                        echo "<script type='text/javascript'>alert('$message');</script>";
                                    } else
                                    {
                                        $name = $rowVal;
                                    }
                                }
                                elseif ('quantity' === $rowKey)
                                {
                                    if($rowVal === '')
                                    {
                                        $message = "forgot to write a value in quantities";
                                        echo "<script type='text/javascript'>alert('$message');</script>";
                                    } else
                                    {
                                        $quantity = $rowVal;
                                    }
                                }
                                elseif ('price' === $rowKey)
                                {
                                    if($rowVal === '')
                                    {
                                        $message = "forgot to write a value in prices";
                                        echo "<script type='text/javascript'>alert('$message');</script>";
                                    } else
                                    {
                                        $price = $rowVal;
                                    }
                                }
                            }

                            if ($_SESSION['groupId'] !== '')
                            {
                                 $row = new Row(null,$_SESSION['groupId'],$name,$quantity,$price);
                                 $this->rowRepository->saveRow($row);
                            }
                            else
                            {
                                die('problem z zapisem wiersza - nie zapisana wczesniej grupa');
                            }
                        }
                    }
                }
                header('Location: index.php?action=guest_panel');
                break;
            case Routing::EDIT :
                $offer = new Offer(
                    $_GET['offer_id'],
                    null,
                    null
                );
                $offer = $this->offerRepository->getOfferById($offer);

//              PRÓBA ZAPISU DO BAZY (zmiana nazwy);

//                if (isset($_POST['review_edit'])) {
//                    $offer = new Offer(
//                        $_POST['review_id'],
//                        $_POST['review_name'],
//                        $_POST['review_city'],
//
//                    );
//                    $this->offerRepository->updateOffer($offer);
//                }

                $group = new Group(
                    null,
                    $_GET['offer_id'],
                    null
                );
                $group = $this->groupRepository->getGroupsByOfferId($group);
                $this->params['offer'] = $offer;
                $this->params['groups'] = $group;
                $this->templateName = 'offer_edit.tmpl.html';
                break;
            case Routing::DELETE :
                $offer = new Offer(
                    $_GET['offer_id'],
                    null,
                    null
                );
                $this->offerRepository->deleteOffer($offer);
                header('Location: index.php?action=guest_panel');
                break;
                break;
            case Routing::PM :
                $this->templateName = 'pm.tmpl.html';
                break;
            case Routing::ADMIN :
                $this->templateName = 'admin.tmpl.html';
                break;
            case Routing::LOGOUT :
                $this->logout();
                break;
            default :
                Response::send('brak strony błąd 404', Response::STATUS_NOT_FOUND);
                break;
        }
        $this->templateEngine->renderTemplate($this->templateName, $this->params);
    }

    ///////////////////////////// LOGOWANIE i REJESTRACJA /////////////////////////////

    private function login()
    {
        unset($_SESSION['username']);

        if(array_key_exists('username', $_POST) && array_key_exists('password', $_POST))
        {
            $user = new Users(
                htmlspecialchars($_POST['username']),
                htmlspecialchars($_POST['password'])
//
            );
            if($this->usersRepo->validateUser($user) === false) {
                $this->params['errors'] = 'Niepoprawny login lub hasło!';
                return;
            } else {
                $this->session->saveToSession('username', $_POST['username']);
                $this->templateName = 'redirToOffer.tmpl.html';
                return;
            }
        }
        if(array_key_exists('form_sent', $_POST)){
            $this->params['errors'] = 'Podaj login i hasło!';
        }
    }

    private function register()
    {
        if (array_key_exists('username', $_POST) && array_key_exists('password', $_POST) &&
            $_POST['username'] != '' &&
            $_POST['user_email'] != '' &&
            $_POST['password'] != '')
        {
            $registeredUsers = new RegisteredUsers(
                null,
                htmlspecialchars($_POST['username']),
                htmlspecialchars($_POST['password']),
                htmlspecialchars($_POST['user_email'])
            );

            if($this->registeredUsersRepo->loginAlreadyExists($registeredUsers) === true)
            {
                $this->params['errors'] = 'Użytkownik o podanym loginie istnieje już w bazie!';
                return;
            }
            elseif($this->registeredUsersRepo->emailAlreadyExists($registeredUsers) === true)
            {
                $this->params['errors'] = 'Podany adres e-mail istnieje już w bazie!';
                return;
            }
            elseif($this->registeredUsersRepo->addNewUser($registeredUsers) === true)
            {
                $this->session->saveToSession('username', $registeredUsers->getUsername());
                header('Location: index.php?action=ajax_index');
            }
            else
            {
                $this->params['errors'] = 'Podany adres e-mail nie jest poprawny!';
                return;
            }
        }
        $this->params['errors'] = 'Wypełnij wszystkie pola danymi!';

    }

    private function logout()
    {
        session_destroy();
        header('Location: index.php?action=index');
    }




}