<?php

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
//use Sda\ConstructionBidForm\Tools\Tools;
use Sda\ConstructionBidForm\Config\Config;
use Sda\ConstructionBidForm\Config\Routing;
use Sda\ConstructionBidForm\Session\Session;
use Sda\ConstructionBidForm\WithRepositories\Users\UsersRepository;
use Sda\ConstructionBidForm\WithRepositories\Users\RegisteredUsersRepository;
use Sda\ConstructionBidForm\WithRepositories\Offers\OfferRepository;
use Sda\ConstructionBidForm\WithRepositories\Groups\GroupRepository;
use Sda\ConstructionBidForm\WithRepositories\Rows\RowRepository;
use Sda\ConstructionBidForm\View\Template;
use Sda\ConstructionBidForm\Request\Request;
use Sda\ConstructionBidForm\Controller\Controller;

require_once __DIR__ . '/../vendor/autoload.php';

$config = new Configuration();
$dbh = \Doctrine\DBAL\DriverManager::getConnection(config::DB_CONNECTION_DATA, $config);

$request = new Request();
$session = new Session();
$userRepo = new UsersRepository($dbh);
$registeredUserRepo = new RegisteredUsersRepository($dbh);
$offerRepo = new offerRepository($dbh);
$groupRepo = new GroupRepository($dbh);
$rowRepo = new RowRepository($dbh);

$loader = new Twig_Loader_Filesystem(Routing::TEMPLATE_DIR);
$twig = new Twig_Environment($loader, [false] );
$template = new Template($twig);

$app = new Controller($request,$session,$userRepo,$registeredUserRepo,$offerRepo,$groupRepo,$rowRepo, $template);
$app->run();




