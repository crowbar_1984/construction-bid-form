<?php

namespace Sda\ConstructionBidForm\View;

use Twig_Environment;

class Template
{
    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * Template constructor.
     * @param Twig_Environment $twig
     */
    public function __construct(Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function renderTemplate($templateName, array $parameters)
    {
        /** @noinspection ExceptionsAnnotatingAndHandlingInspection */
        echo $this->twig->render($templateName, $parameters);
    }

}