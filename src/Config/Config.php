<?php

namespace Sda\ConstructionBidForm\Config ;

class Config
{
    const DB_CONNECTION_DATA =
        [
            'dbname' => 'construction',
            'user' => 'root',
            'password' => '',
            'host' => 'localhost',
            'driver' => 'pdo_mysql',
        ];

}
