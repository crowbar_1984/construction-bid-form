<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 2017-04-25
 * Time: 19:56
 */

namespace Sda\ConstructionBidForm\Config;

class Routing
{
    const DELETE = 'delete_offer';
    const EDIT = 'edit_offer';
    const INDEX = 'index';
    const LOGIN = 'login';
    const REGISTER = 'register';
    const LOGOUT = 'logout';
    const NEW_OFFER = 'new_offer';
    const GUEST_PANEL = 'guest_panel';
    const PM = 'project manager';
    const INDEX_AJAX = 'ajax_index';
    const SAVE_FORM = 'save_form';

    const ADMIN = 'admin';
    const TEMPLATE_DIR = __DIR__ . '/../View/templates';
}