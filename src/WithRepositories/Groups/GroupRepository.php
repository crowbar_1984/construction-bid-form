<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 2017-05-11
 * Time: 19:10
 */

namespace Sda\ConstructionBidForm\WithRepositories\Groups;

use Doctrine\DBAL\Connection;

class GroupRepository
{
    private $dbh;

    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    public function saveGroup(Group $group)
    {
        $this->dbh->insert('groups', [
            'offer_id' => $group->getOfferId(),
            'group_name' => $group->getName(),
        ]);

        $id = $this->dbh->lastInsertId();
        $group->setId($id);

        return $group;
    }

    public function getGroupsByOfferId(Group $group)
    {
        $sth = $this->dbh->prepare
        (
            'SELECT * FROM `groups` WHERE `offer_id` = :id'
        );
        $sth->bindValue('id', $group->getOfferId(),\PDO::PARAM_INT);
        $sth->execute();

        $offerData = $sth->fetchAll();

        return $offerData;
    }

    /////////// METODA JESZCZE NIE ZAIMPLEMENTOWANA //////////////////

    public function updateGroup(Group $group)
    {
        return $this->dbh->update('groups', array(
            'offer_id' => $group->getOfferId(),
            'group_name' => $group->getName()
        ), array(
            'group_id' => $group->getId()
        ));
    }



}