<?php

namespace Sda\ConstructionBidForm\WithRepositories\Groups;

class Group
{

    private $id;
    private $offerId;
    private $name;



    /**
     * Group constructor.
     * @param $id
     * @param $name
     */
    public function __construct($id,$offerId ,$name)
    {
        $this->id = $id;
        $this->offerId = $offerId;
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getOfferId()
    {
        return $this->offerId;
    }

}