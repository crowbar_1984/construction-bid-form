<?php

namespace Sda\ConstructionBidForm\WithRepositories\Offers;

use Sda\ConstructionBidForm\TypedCollection;

class OfferCollection extends TypedCollection
{
    public function __construct()
    {
        $this->setItemType(offer::class);
    }
}