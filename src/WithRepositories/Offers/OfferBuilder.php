<?php

namespace Sda\ConstructionBidForm\WithRepositories\Offers;

class offerBuilder
{
    /**
     * @var int
     */
    private $id;
    private $usersId;
    private $name;

    /**
     * @return offer
     */
    public function build()
    {
        return new Offer(
            $this->id,
            $this->usersId,
            $this->name
            );
    }

    /**
     * @param int $id
     * @return $this
     */
    public function withId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function withUserId($usersId)
    {
        $this->usersId = $usersId;
        return $this;
    }

    /**
     * @param string $name
     */
    public function withName($name)
    {
        $this->name = $name;
        return $this;
    }



}