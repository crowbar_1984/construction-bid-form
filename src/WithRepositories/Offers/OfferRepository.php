<?php

namespace Sda\ConstructionBidForm\WithRepositories\Offers;

use Doctrine\DBAL\Connection;

class offerRepository
{

    private $dbh;

    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param $usersId
     * @return mixed
     */
    public function getOffersFromUser($usersId)
    {
        $sth = $this->dbh->prepare
        (
            'SELECT * FROM `offers` WHERE `user_id` = :id'
        );
        $sth->bindValue('id', $usersId,\PDO::PARAM_INT);
        $sth->execute();
        
        $offersData = $sth->fetchAll();

        return $offersData;
    }

    public function getOfferById (Offer $offer) {
        $sth = $this->dbh->prepare
        (
            'SELECT * FROM `offers` WHERE `offer_id` = :id'
        );
        $sth->bindValue('id', $offer->getId(),\PDO::PARAM_INT);
        $sth->execute();

        $offerData = $sth->fetch();

        return $offerData;
    }


    public function saveOffer(Offer $offer)
    {
        $this->dbh->insert('offers', [
            'user_id' => $offer->getUsersId(),
            'offer_name' => $offer->getName()
        ]);

        $id = $this->dbh->lastInsertId();
        $offer->setId($id);

        return $offer;
    }

//    TU chcę żeby zadziałał update

    public function updateOffer(Offer $offer)
    {
        return $this->dbh->update('offers', array(
            'user_id' => $offer->getUsersId(),
            'offer_name' => $offer->getName()
        ), array(
            'offer_id' => $offer->getId()
        ));


    }



    public function offersIdByName($offerName)
    {
        $sth = $this->dbh->prepare
        (
            'SELECT `offer_id` FROM `offers` WHERE `offer_name` = :offer_name'
        );
        $sth->bindValue('offer_name', $offerName,\PDO::PARAM_INT);
        $sth->execute();

        $data = $sth->fetch();

        return $data;
    }


    public function deleteOffer(Offer $offer)
    {
        return $this->dbh->delete('offers', array(
            'offer_id' => $offer->getId()
        ));
    }

}