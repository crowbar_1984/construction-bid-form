<?php

namespace Sda\ConstructionBidForm\WithRepositories\Offers;

class Offer
{
    private $id;
    private $usersId;
    private $name;

    /**
     * offer constructor.
     * @param $id
     * @param $userId
     * @param $name
     */
    public function __construct($id, $usersId, $name)
    {
        $this->id = $id;
        $this->usersId = $usersId;
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsersId()
    {
        return $this->usersId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


}