<?php

namespace Sda\ConstructionBidForm\WithRepositories\Offers;

class offerFactory
{
    /**
     * @param array $row
     * @return mixed
     */
    public static function makeFromOffersRepository(array $row)
    {
        $builder = new offerBuilder();

        return $builder
            ->withId((int)$row['offer_id'])
            ->withUserId((int)$row['users_id'])
            ->withName($row['offer_name'])
            ->buid();  // metoda nie inicjuje się samoistnie
    }
}