<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 2017-05-10
 * Time: 13:17
 */

namespace Sda\ConstructionBidForm\WithRepositories\Users;


/**
 * Class RegistredUsersBuilder
 * @package Sda\ConstructionBidForm\WithRepositories\Users
 */
class RegistredUsersBuilder
{
    /**
     * @var
     */
    private $id;
    /**
     * @var
     */
    private $username;
    /**
     * @var
     */
    private $password;
    /**
     * @var
     */
    private $email;
    /**
     * @var
     */
    private $perms;

    /**
     * @return RegisteredUsers
     */
    public function build()
    {
        return new RegisteredUsers(
            $this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->perms
        );
    }

    /**
     * @param $id
     * @return $this
     */
    public function withId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param $username
     * @return $this
     */
    public function withUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @param $password
     * @return $this
     */
    public function withPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param $email
     * @return $this
     */
    public function withEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @param $perms
     * @return mixed
     */
    public function withPerms($perms)
    {
        $this->perms = $perms;
        return $perms;
    }

}