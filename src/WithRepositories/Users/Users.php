<?php

namespace Sda\ConstructionBidForm\WithRepositories\Users;

class users
{
    private $username;
    private $pass;

    /**
     * users constructor.
     * @param $id
     * @param $login
     * @param $pass
     * @param $email
     */
    public function __construct($username, $pass)
    {
        $this->username = $username;
        $this->pass = $pass;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }


}