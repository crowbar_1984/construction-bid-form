<?php

namespace Sda\ConstructionBidForm\WithRepositories\Users;

/**
 * Class usersBuilder
 * @package Sda\ConstructionBidForm\WithRepositories\Users
 */
class usersBuilder
{
    /**
     * @var
     */
    private $username;
    /**
     * @var
     */
    private $pass;

    /**
     * @return users
     */
    public function build()
    {
        return new users
        (
            $this->username,
            $this->pass
        );
    }

    /**
     * @param $username
     * @return $this
     */
    public function withUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @param $pass
     * @return $this
     */
    public function withPass($pass)
    {
        $this->pass = $pass;
        return $this;
    }

}