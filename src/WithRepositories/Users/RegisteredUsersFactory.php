<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 2017-05-10
 * Time: 15:38
 */

namespace Sda\ConstructionBidForm\WithRepositories\Users;


class RegisteredUsersFactory
{
    /**
     * @param array $row
     * @return mixed
     */
    public static function makeFromRegisteredUsersRepository (array $row) {

        $builder = new RegistredUsersBuilder();

        return $builder
            ->withId($row['user_id'])
            ->withUsername($row['user_name'])
            ->withPassword(($row['user_pass']))
            ->withEmail($row['user_email'])
            ->withPerms($row['user_perms'])
            ->build();
    }
}