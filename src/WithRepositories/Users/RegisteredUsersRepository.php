<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 2017-05-10
 * Time: 15:45
 */

namespace Sda\ConstructionBidForm\WithRepositories\Users;

use Doctrine\DBAL\Connection;

class RegisteredUsersRepository

{
    private $dbh;

    /**
     * RegisteredUsersRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param RegisteredUsers $registeredUser
     * @return bool
     */
    public function loginAlreadyExists(RegisteredUsers $registeredUser)
    {
        $sth = $this->dbh->prepare('SELECT `user_name`, `user_pass` FROM `users` WHERE `user_name` = :username');
        $sth->bindValue('username', $registeredUser->getUsername(), \PDO::PARAM_STR);
        $sth->execute();
        $user = $sth->fetch();

        return ($user != false);
    }

    /**
     * @param RegisteredUsers $registeredUser
     * @return bool
     */
    public function emailAlreadyExists(RegisteredUsers $registeredUser)
    {
        $sth = $this->dbh->prepare('SELECT `user_email`, `user_pass` FROM `users` WHERE `user_email` = :user_email');
        $sth->bindValue('user_email', $registeredUser->getEmail(), \PDO::PARAM_STR);
        $sth->execute();
        $user = $sth->fetch();

        return ($user != false);
    }

    /**
     * @param RegisteredUsers $registeredUser
     * @return bool
     */
    public function addNewUser(RegisteredUsers $registeredUser)
    {

        if($_POST['username'] !== false && strpos($_POST['user_email'], '@') !== false && isset($_POST['password']) === true)
        {
            $hashedPassword = crypt($registeredUser->getPassword(), uniqid('', true));
            $dataToInsert = $this->dbh->insert(
                'users', [
                'user_name' => $registeredUser->getUsername(),
                'user_email' => $registeredUser->getEmail(),
                'user_pass' => $hashedPassword,
                'user_perms' => $registeredUser->getPerms()
            ]);
            return true;
        }
        return false;
    }

    /**
     * @param $userName
     * @return mixed
     */
    public function getUserIdByName($userName)
    {
        $sth = $this->dbh->prepare('SELECT `user_id` FROM `users` WHERE `user_name` = :username');
        $sth->bindValue('username', $userName, \PDO::PARAM_STR);
        $sth->execute();
        $data = $sth->fetch();

        return $data["user_id"];
    }
}