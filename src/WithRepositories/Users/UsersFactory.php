<?php

namespace Sda\ConstructionBidForm\WithRepositories\Users;

class usersFactory
{
    /**
     * @param array $row
     * @return users
     */
    public static function makeFromUsersRepository(array $row)
    {
        $builder = new usersBuilder();

        return $builder
            ->withUsername($row['user_name'])
            ->withPass($row['user_pass'])
            ->build();
    }
}