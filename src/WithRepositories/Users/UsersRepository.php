<?php

namespace Sda\ConstructionBidForm\WithRepositories\Users;

use Doctrine\DBAL\Connection;

class UsersRepository
{
    private $dbh;

    /**
     * usersRepository constructor.
     * @param $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param Users $users
     * @return bool
     */
    public function validateUser(Users $users)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `users` WHERE `user_name` = :user_name');
        $sth->bindValue('user_name', $users->getUsername(), \PDO::PARAM_STR);
        $sth->execute();
        $validatedUser = $sth->fetch();

        if(false !== $validatedUser && hash_equals($validatedUser['user_pass'], crypt($users->getPass(), $validatedUser['user_pass'])))
        {
            return true;
        }
        return false;
    }

}