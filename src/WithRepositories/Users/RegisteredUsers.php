<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 2017-05-10
 * Time: 13:11
 */

namespace Sda\ConstructionBidForm\WithRepositories\Users;


class RegisteredUsers
{
    private $id;
    private $username;
    private $password;
    private $email;
    private $perms;

    /**
     * RegisteredUsers constructor.
     * @param $id
     * @param $username
     * @param $password
     * @param $email
     * @param $perms
     */
    public function __construct($id, $username, $password, $email, $perms = 0)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->email = $email;
        $this->perms = $perms;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPerms()
    {
        return $this->perms;
    }


}