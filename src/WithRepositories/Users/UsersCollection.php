<?php

namespace Sda\ConstructionBidForm\WithRepositories\Users;

use Sda\ConstructionBidForm\TypedCollection;

class usersCollection extends TypedCollection
{
    /**
     * usersCollection constructor.
     */
    public function __construct()
    {
        $this->setItemType(users::class);
    }

}