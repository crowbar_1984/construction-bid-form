<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 2017-04-27
 * Time: 17:39
 */

namespace Sda\ConstructionBidForm\WithRepositories\Rows;


class RowBuilder
{
    /**
     * @var
     */
    private $id;
    /**
     * @var
     */
    private $offersId;
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $quantity;
    /**
     * @var
     */
    private $price;


    /**
     * @return Row
     */
    public function build()
    {
        return new Row
        (
            $this->id,
            $this->offersId,
            $this->name,
            $this->quantity,
            $this->price
        );
    }

    /**
     * @param int $id
     * @return $this
     */
    public function withId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param int $offersId
     * @return $this
     */
    public function withOffersId($offersId)
    {
        $this->offersId = $offersId;
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function withName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param $quantity
     * @return $this
     */
    public function withQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @param $price
     * @return $this
     */
    public function withPrice($price)
    {
        $this->price = $price;
        return $this;
    }






}