<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 2017-04-27
 * Time: 17:55
 */

namespace Sda\ConstructionBidForm\WithRepositories\Rows;

use Doctrine\DBAL\Connection;

class RowRepository
{
    private $dbh;

    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param Row $row
     * @return Row
     */
    public function saveRow(Row $row)
    {
        $this->dbh->insert('rows', [
            'row_form_name' => $row->getName(),
            'group_id' => $row->getGroupId(),
            'row_form_quantity' => $row->getQuantity(),
            'row_form_price' => $row->getPrice(),
        ]);

        $id = $this->dbh->lastInsertId();
        $row->setId($id);

        return $row;
    }

}