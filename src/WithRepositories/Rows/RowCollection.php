<?php

namespace Sda\ConstructionBidForm\WithRepositories\Rows;

use Sda\ConstructionBidForm\TypedCollection;

class RowCollection extends TypedCollection
{
    /**
     * RowCollection constructor.
     */
    public function __construct()
    {
        $this->setItemType(Row::class);
    }
}