<?php

namespace Sda\ConstructionBidForm\WithRepositories\Rows;

class Row
{
    private $id;
    private $groupId;
    private $name;
    private $price;
    private $quantity;

    /**
     * offersForm constructor.
     * @param $id
     * @param $offersId
     * @param $name
     * @param $quantity
     * @param $price
     */
    public function __construct($id, $groupId, $name, $quantity, $price)
    {
        $this->id = $id;
        $this->groupId = $groupId;
        $this->name = $name;
        $this->quantity = $quantity;
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }




}