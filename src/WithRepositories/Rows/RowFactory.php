<?php

namespace Sda\ConstructionBidForm\WithRepositories\Rows;

class rowFactory
{
    /**
     * @param array $row
     * @return Row
     */
    public static function makeFromOfferFormRepository(array $row)
    {
        $builder = new RowBuilder();

        return $builder
                ->withId($row['offers_form_id'])
                ->withOffersId($row['offers_id'])
                ->withName($row['offers_form_name'])
                ->withQuantity($row['offers_form_quantity'])
                ->withPrice($row['offers_form_price'])
                ->build();
    }

}