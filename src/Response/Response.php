<?php

namespace Sda\ConstructionBidForm\Response;

class Response {
    
    const STATUS_OK = 200;
    const STATUS_BAD_REQUEST = 400;
    const STATUS_PERMISION_DENIED = 403;
    const STATUS_NOT_FOUND = 404;

    public static function send($dataToSend, $statusCode = self::STATUS_OK, $errors =[]){
        
        http_response_code($statusCode);

        $dataToSend = [
            'errors'=>$errors,
            'data'=>$dataToSend
        ];

        echo json_encode($dataToSend);
        exit();
    }

}