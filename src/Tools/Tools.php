<?php

namespace Sda\ConstructionBidForm\Tools;

abstract class Tools
{
    /**
     * @param $data
     * @param $type
     */
    public static function preFormat($data, $type)
    {
        echo '<pre>';

        switch($type) {
            case 'v':
                var_dump($data);
                break;
            case 'p':
                print_r($data);
                break;
            default:
                var_dump($data);
        }

        echo '</pre>';
    }


}