/// JavaScript $ jQuery /////

var main = {
    groupId: 1,
    rowId: 1,
    startup: function () {
        $('#offer_block').append(
            $(main.renderGroup(main.groupId++, false))
        );

        /// TO Ta metoda ////

        // $('#offer_edited_block').append(
        //     $(main.renderEditedGroup(true))
        // );

        ///////

        main.registerForm();
        main.loginForm();
        main.bindGroup();
        main.bindRow();
        main.listMenu();
    },

    listMenu: function () {
        $(document).ready(function () {
            $('.dropbtn').click(function () {
                $('.dropdown-content').slideToggle(100);
            });
        });
    },

    redirTo: function (url) {
        window.location=url;
    },

    registerForm: function () {
        $(document).on('click', '#register', function () {
            $.ajax({
                url: "index.php?action=register",
                type: 'GET',
                success: function (returnData) {
                    $('#content').html(returnData);
                }
            });
        });
    },

    bindRegisterForm: function() {
        $('#register_form').unbind("submit");

        $('#register_form').submit(function (e) {
            var loginForm = $(this);
            $.ajax({
                url: "index.php?action=register",
                type: 'POST',
                data: loginForm.serialize(),
                success: function (returnData) {
                    $('#content').html(returnData);
                }
            });
            e.preventDefault();
        });
    },

    loginForm: function () {

        $(document).on('click', '#navLogin', function () {
            $.ajax({
                url: "index.php?action=login",
                type: 'GET',
                success: function (returnData) {
                    $('#content').html(returnData);
                }
            });
        });

    },

    bindloginForm: function () {
        $('#login_form').unbind("submit");

        $('#login_form').submit(function (e) {
            var loginForm = $(this);
            $.ajax({
                url: "index.php?action=login",
                type: 'POST',
                data: loginForm.serialize(),
                success: function (returnData) {
                    $('#content').html(returnData);
                }
            });
            e.preventDefault();
        });
    },

    bindInputs: function () {
        $("input.CountValue").unbind("change");
        $(document).on('change', 'input.CountValue', function () {
            var input = $(this).attr('id');
            var idx = input.replace('input_', '').split('_');
            main.calculateRow(idx[0], idx[1]);
        });
    },

    bindRow: function () {
        $(document).on('click', '.btn_add', function (e) {
            var button_id = $(this).attr('id');
            var idx = button_id.replace('btn_add_row_', '').split('_');

            $("#row" + idx[1]).after(
                $(main.renderRow(main.rowId++, idx[0], true))
            );

            main.bindInputs();

            e.preventDefault();
        });

        $(document).on('click', '.btn_remove', function (e) {

            var button_id = $(this).attr('id');
            var idx = button_id.replace('btn_del_row_', '').split('_');

            $("#row" + idx[1]).remove();

            main.calculateGroup(idx[0]);

            main.bindInputs();

            e.preventDefault();

        });

        main.bindInputs();
    },

    bindGroup: function () {
        $(document).on('click', '.btn_add_group', function (e) {
            var button_id = $(this).attr("id");
            var x = button_id.replace('btn_add_group_', '');


            $("#group" + x).after(
                $(main.renderGroup(main.groupId++, true))
            );

            main.bindInputs();

            e.preventDefault();
        });

        $(document).on('click', '.btn_remove_group', function (e) {

            e.preventDefault();
            var button_id = $(this).attr("id");
            var x = button_id.replace('btn_del_group_', '');

            $("#group" + x).remove();

            main.calculateTotal();
            main.bindInputs();

        });
    },

    renderRow: function (i, j, render_del) {
        var content = '<div id="row' + i + '" class="row">';
        content += '<div class="col-xs-8 col-sm-8">';
        content += '<input placeholder="Item name" name="offerData[' + j + '][' + i + '][name]" type="text" class="col-xs-6 col-sm-6">';
        content += '<input id="input_' + j + '_' + i + '_1" placeholder="qu." name="offerData[' + j + '][' + i + '][quantity]" type="number" class="CountValue CountValue1 col-xs-2 col-sm-1 paddRightOff">';
        content += '<input id="input_' + j + '_' + i + '_2" placeholder="pr." name="offerData[' + j + '][' + i + '][price]" type="number" class="CountValue CountValue2 col-xs-2 col-sm-1 paddRightOff">';
        content += '<div id="result_for_group_' + j + '_' + i + '" class="form result col-xs-4 col-sm-4"> zł </div>';
        content += '</div>';
        content += '<section class="col-xs-3 col-sm-3">';
        content += '<button id="btn_add_row_' + j + '_' + i + '" class="btn btn_add"> + ROW </button>';

        if (render_del === true) {
            content += '<button id="btn_del_row_' + j + '_' + i + '" class="btn btn_remove"> - ROW </button>';
        }
        content += '</section>';
        content += '<section class="col-xs-1 col-sm-1"></section>';
        content += '</div>';

        return content;
    },

    renderGroup: function (j, render_del_group) {
        var content = '<div id="group' + j + '" class="offText">';
        content += '<div class="row group">';
        content += '<div class="col-xs-8 col-sm-8">';
        content += '<input placeholder="Group name" name="offerData[' + j + '][group_name]" type="text" class="col-xs-8 col-sm-8">';
        content += '<div class="form col-xs-4 col-sm-4 group_result"> zł </div>';
        content += '</div>';
        content += '<section class="col-xs-3 col-sm-3">';
        content += '<button id="btn_add_group_' + j + '" class="btn btn_add_group">  + GROUP ROW </button>';

        if (render_del_group === true) {
            content += '<button id="btn_del_group_' + j + '" class="btn btn_remove_group">  - GROUP ROW </button>';
        }

        content += '</section>';
        content += '<section class="col-xs-1 col-sm-1"></section>';
        content += '</div>';

        content += '<div class="row">';
        content += '<div id="table" class="col-xs-8 col-sm-8">';
        content += '<div class="col-xs-6 col-sm-6"> NAME </div>';
        content += '<div class="col-xs-1 col-sm-1"> QUANT. </div>';
        content += '<div class="col-xs-1 col-sm-1"> PRICE </div>';
        content += '<div class="col-xs-4 col-sm-4"> TOTAL VALUE </div>';
        content += '</div>';
        content += '<section class="col-xs-4 col-sm-4"></section>';
        content += '</div>';
        content += main.renderRow(main.rowId++, j, false);

        return content;
    },

    calculateRow: function (groupId, rowId) {
        var row = $('#row' + rowId);

        // sumowanie 'li-ków' ///

        var n1 = row.find('.CountValue1').val();
        var n2 = row.find('.CountValue2').val();

        var res = parseFloat(n1) * parseFloat(n2) + ' zł';

        if (n1 !== '' && n2 !== '') {
            row.find('.result').text(res);
        }
        main.calculateGroup(groupId);
    },

    calculateGroup: function (groupId) {
        var groupPath = $('#group' + groupId);
        var element = '';
        var sum = 0;

        groupPath.find(".result").each(function () {
            element = $(this).text();
            var elementRepl = parseFloat(element.replace(' zł', ''));
            sum += elementRepl || 0;
        });

        groupPath.find('.group_result').text(sum + ' zł');

        main.calculateTotal();
    },

    calculateTotal: function () {

        var element = '';
        var sum = 0;

        $(".group_result").each(function () {
            element = $(this).text();
            var elementRepl = parseFloat(element.replace(' zł', ''));
            sum += elementRepl || 0;
        });

        $('#view_sum').html('<h3 style="margin: 0; text-align: right;">' + sum + ' zł </h3>');

    },

    renderEditedGroup : function (render_del_group) {

        var content = '{% for group in groups %}';

        content += '<div id="group{{ group.group_id }}" class="offText">';
        content += '<div class="row group">';
        content += '<div class="col-xs-8 col-sm-8">';
        content += '<input value="" name="offerData[{{ group.group_id }}][group_name]" type="text" class="col-xs-8 col-sm-8">';
        content += '<div class="form col-xs-4 col-sm-4 group_result"> zł </div>';
        content += '</div>';
        content += '<section class="col-xs-3 col-sm-3">';
        content += '<button id="btn_add_group_{{ group.group_id }}" class="btn btn_add_group">  + GROUP ROW </button>';

        if (render_del_group === true) {
            content += '<button id="btn_del_group_{{ group.group_id }}" class="btn btn_remove_group">  - GROUP ROW </button>';
        }

        content += '</section>';
        content += '<section class="col-xs-1 col-sm-1"></section>';
        content += '</div>';

        content += '<div class="row">';
        content += '<div id="table" class="col-xs-8 col-sm-8">';
        content += '<div class="col-xs-6 col-sm-6"> NAME </div>';
        content += '<div class="col-xs-1 col-sm-1"> QUANT. </div>';
        content += '<div class="col-xs-1 col-sm-1"> PRICE </div>';
        content += '<div class="col-xs-4 col-sm-4"> TOTAL VALUE </div>';
        content += '</div>';
        content += '<section class="col-xs-4 col-sm-4"></section>';
        content += '</div>';
        // content += main.renderRow(main.rowId++,{{ group.group_id }}, false);
        content += '{% endfor %}';

        return content;
    }

};

$(document).ready(function (){
   main.startup();
});

