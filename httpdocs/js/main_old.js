/// js /////

var main = {
    statup: function(){

    },
    renderGroup: function () {

    }
};




function renderGroup(i,j,render_del_group,button) {

 var content = '<div id="group'+j+'" class="offText">';
        content += '<div class="row group">';
            content += '<div class="col-xs-8 col-sm-8">';
                content += '<input placeholder="Group name" name="form_group[1][name]" type="text" class="col-xs-8 col-sm-8">';
                content += '<div class="form col-xs-4 col-sm-4 group_result" placeholder="wynik"> zł </div>';
            content += '</div>';
            content += '<section class="col-xs-3 col-sm-3">';
                content += '<button id="btn_add_group_'+j+'"class="btn btn_add_group">  + GROUP ROW </button>';

    if (render_del_group === true) {
                content += '<button id="' + j + '" class="btn btn_remove_group">  - GROUP ROW </button>';
    }

            content += '</section>';
            content += '<section class="col-xs-1 col-sm-1"></section>';
        content += '</div>';

        content += '<div class="row">';
            content += '<div id="table" class="col-xs-8 col-sm-8">';
            content += '<div type="text" class="col-xs-6 col-sm-6"> NAZWA </div>';
            content += '<div type="text" class="col-xs-1 col-sm-1"> ILOSC </div>';
            content += '<div type="text" class="col-xs-1 col-sm-1"> C. J. </div>';
            content += '<div type="text" class="col-xs-4 col-sm-4"> WARTOŚC </div>';
        content += '</div>';
        content += '<section class="col-xs-4 col-sm-4"></section>';
        // czy wbić tutaj (renderRow) czy jako content ?????
    content += '</div>';

    if (button) {
         $(content).insertAfter($(button).parents('.offText'));
        if (i === 1 && j === 1) {
            renderRow( i, j, false, false);
        } else {
            renderRow( i, j, true, true);
        }
        return;
    }

    $('.offer').append(content);

    if (i === 1 && j === 1) {
    renderRow( i, j, false, false);
    } else {
        renderRow( i, j, true, true);
    }
}

function renderRow(i,j,render_del, button) {

 var content = '<div id="row'+i+'" class="row">';
        content += '<div class="col-xs-8 col-sm-8">';
            content += '<input placeholder="nazwa pozycji" name="form[1][name]" type="text" class="col-xs-6 col-sm-6">';
            content += '<input placeholder="ilosc" name="form[2][quantity]" type="number" class="CountValue CountValue1 col-xs-2 col-sm-1 paddRightOff">';
            content += '<input placeholder="c.j." name="form[3][price]" type="number" class="CountValue CountValue2 col-xs-2 col-sm-1 paddRightOff">';
            content += '<div id="result_for_group_'+j+'" class="form result col-xs-4 col-sm-4" placeholder="wynik"> zł </div>';
        content += '</div>';
        content += '<section class="col-xs-3 col-sm-3">';
            content += '<button id="btn_add_'+i+'" class="btn btn_add"> + ROW </button>';

    if (render_del === true) {
            content += '<button id="'+i+'" class="btn btn_remove"> - ROW </button>';
    }
        content += '</section>';
        content += '<section class="col-xs-1 col-sm-1"></section>';
    content += '</div>';

    if (button) {
        return $(content).insertAfter($(button).parents('.row'));
    }

    $(content).appendTo('#group'+j);
}

/// jQuery ////

//                                //
// -- dodawanie wierszy i grup -- //
//                                //

$(document).ready(function ()
{
    var j = 1;
    var i = 1;

    $(document).on('click', '.btn_add_group' ,function (e)
    {
        var button_id = $(this).attr("id");
        var x = button_id.replace('btn_add_group_', '');


        var buttons = $('.btn_add_group');
        var max_button = buttons[0];

        buttons.each(function(){
            var actual = parseInt($(this).attr('id').replace('btn_add_group_', ''));
            var actual_max = parseInt($(max_button).attr('id').replace('btn_add_group_', ''));
            if (actual > actual_max) {
                max_button = this;
            }
        });


        j++;

        content = renderGroup(i,j,true, max_button);
        $("#group" + x).after(content);

        e.preventDefault();
    });

    $(document).on('click', '.btn_remove_group', function(e)
    {
        e.preventDefault();
        var button_id = $(this).attr("id");
        $("#group" + button_id).remove();
    });




    $(document).on('click', '.btn_add' ,function (e)
    {
        var button_id = $(this).attr('id');
        var x = button_id.replace('btn_add_', '');


        var buttons = $('.btn_add');
        var max_button = buttons[0];

        buttons.each(function(){
            var actual = parseInt($(this).attr('id').replace('btn_add_', ''));
            var actual_max = parseInt($(max_button).attr('id').replace('btn_add_', ''));
            if (actual > actual_max) {
                max_button = this;
            }
        });

        i++;
        content = renderRow(i,j,true, max_button);

        $("#row" + x).after(content);

        e.preventDefault();
    });

    $(document).on('click', '.btn_remove', function(e)
    {

        var button_id = $(this).attr("id");

        $("#row" + button_id).remove();

        e.preventDefault();
    });




    /*
    $('#submit').click(function(){
        $.ajax({
            url:"name.php",
            method:"POST",
            data:$('#add_name').serialize(),
            success:function(data)
            {
                alert(data);
                $('#add_name')[0].reset();
            }
        });
    });
    */

    /*
    $(document).on('click','nav ul li', function (){
        $(this).siblings().removeClass("current")
        $(this).addClass( "current" )
        /*$(this).append('.current');*/
        /*$(".text_change").append(".current");
    });
    */


});

//                 //
// -- sumowanie -- //
//                 //

$(document).ready(function () {

    var arr = [];

    $(document).on('change', 'div.row div input.CountValue', function()
    {
        var row = $(this).parent().parent();
        var rowGroup = $(this).parents('.offText');

        // sumowanie 'li-ków' ///

        var n1 = row.find('.CountValue1').val();
        var n2 = row.find('.CountValue2').val();

        var res = parseFloat(n1) * parseFloat(n2) + ' zł';

        if(n1 !== '' && n2 !== ''){
            row.find('.result').text(res);
        }

        // sumowanie grupowe ///



        // sumowanie całkowite ///

    });

});

// inne //

$(document).ready(function () {

    console.log('dupa');

    $(document).on('click', 'header div div nav ul li a', function(e) {
        e.preventDefault();

        $(this).parent().parent().find('li').removeClass("current");
        $(this).parent().addClass("current");

    });

});

///////////////////////////////
mathGroupAddition: function () {
    $(document).on('change', 'div.row div input.CountValue', function()
    {

        var resultRow = $(this).parent().children('.result').attr('id');
        var resultRowRepl = resultRow.replace('result_for_group_','')
        var resultRowReplSplit = resultRowRepl.split('_');
        var resultRowValue = resultRowReplSplit[0];


        //console.log(resultRowValue);

        var resultGroup = $(this).parents('.offText').attr('id');
        var resultGroupRepl = resultGroup.replace('group','');
        var resultGroupValue = resultGroupRepl[0];

        //console.log(resultGroupValue);


        var rowGroup = $(this).parents('.offText');

        //var resultValueAll = rowGroup.find('.result').html();

        //var arr = [];
        var element = '';
        var sum = 0;

        rowGroup.find(".result").each(function() {
            element = $(this).text();
            var elementRepl = parseFloat(element.replace(' zł', ''));
            sum += elementRepl || 0;
            //arr.push(elementRepl);
        });

        //var sum = 0;
        //$.each(arr,function(){
        //  if( $(this) !== NaN ) {
        //    sum += parseFloat(this) || 0;
        //};
        //});

        var groupPath = $(this).parents('.offText')
        groupPath.find('.group_result').text(sum + ' zł');

    });
}
